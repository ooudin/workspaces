#!/bin/bash

# Cherche tous les projets gérés par GWS
for wksp in * ; do
	if [ -d "$wksp" ]; then
		echo 
		echo "Vérification de $wksp"
		if [ -e $wksp/.projects.gws ] ; then
			cd $wksp
			gws
			cd ..
		else
			echo " => pas un projet GWS "
		fi
	fi
done


