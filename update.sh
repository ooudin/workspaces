#!/bin/bash

# Cherche tous les projets gérés par GWS
for wksp in * ; do
	if [ -d "$wksp" ]; then
		echo 
		echo "Vérification de $wksp"
		if [ -e $wksp/.projects.gws ] ; then
			cd $wksp			
			if [ ! -e .mu_repo ] ; then
				mu register --all
			fi
			mu fetch -p
			gws ff
			cd ..
		else
			echo " => pas un projet GWS "
		fi
	fi
done


