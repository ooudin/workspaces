#!/bin/bash

# Cherche tous les projets gérés par GWS
for wksp in * ; do
	if [ -d "$wksp" ]; then
		echo 
		echo "Vérification de $wksp"
		if [ -e $wksp/.projects.gws ] ; then
			cd $wksp			
			
			# Cherche tous les projets maven
			for proj in * ; do
				echo
				echo "	Vérification de $proj"
				if [ -e $proj/pom.xml ] ; then
					cd $proj
					mvn clean install -DskipTests
					cd ..

				else
					echo "	 => pas un projet maven"
				fi

			done
			cd ..
			
		else
			echo " => pas un projet GWS "
		fi
	fi
done


